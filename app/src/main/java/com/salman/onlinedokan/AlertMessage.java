package com.salman.onlinedokan;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

/**
 * Created by aditya on 5/13/16.
 */
public class AlertMessage {
    public static void showMessage(final Context c, final String title,
                                   final String s) {
        final AlertDialog.Builder aBuilder = new AlertDialog.Builder(c);
        aBuilder.setTitle(title);
        // aBuilder.setIcon(R.drawable.info);
        aBuilder.setMessage(s);

        aBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(final DialogInterface dialog, final int which) {
                dialog.dismiss();
            }

        });

        aBuilder.show();
    }

}
